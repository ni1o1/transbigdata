.. _mobile:
.. currentmodule:: transbigdata

******************************
Mobilephone data processing
******************************

.. autosummary::
   :toctree: api/
   
    mobile_stay_move
    mobile_stay_dutation
    mobile_identify_home
    mobile_identify_work


.. autofunction:: mobile_stay_move

.. autofunction:: mobile_stay_dutation

.. autofunction:: mobile_identify_home

.. autofunction:: mobile_identify_work




